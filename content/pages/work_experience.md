+++
title = "Work Experience"
path = "work_experience"
+++

## Full-Time Experience
### <span style="color:#00aeef">Barclays </span>
- <span style="color:#00aeef">Data Analyst </span>
- <span style="color:#00aeef">Whippany, NJ </span>
- <span style="color:#00aeef">2020 July ~ 2023 May </span>


## Inernship Experience
### <span style="color:#00aeef">ScholarJet </span>
- <span style="color:#00aeef">Marketing Data Analyst Intern </span>
- <span style="color:#00aeef">Boston, MA </span>
- <span style="color:#00aeef">2019 July ~ 2019 August </span>

### <span style="color:#00aeef">ScholarJet </span>
- <span style="color:#00aeef">Marketing and Communications Intern </span>
- <span style="color:#00aeef">Boston, MA </span>
- <span style="color:#00aeef">2019 Sepeteber ~ 2019 December </span>


### <span style="color:#00aeef">Beijing ByteDance Technology </span>
- <span style="color:#00aeef">Data Analyst Intern </span>
- <span style="color:#00aeef">Beijing, China </span>
- <span style="color:#00aeef">2018 July ~ 2018 August </span>

