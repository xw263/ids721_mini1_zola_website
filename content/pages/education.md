+++
title = "Education"
path = "education"
+++

## Master's Degree
Duke University
- Master's in Data Science

## Bachelor's Degree
Boston University
- Statistics
- Psychology