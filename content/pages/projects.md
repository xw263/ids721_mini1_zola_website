+++
title = "Projects"
path = "projects"
+++

### Data Science

#### Youtube Revenue Prediction
The team project, "Youtube Revenue Prediction" is to predict the revenue for each youtube channel. 



### Data Engineering



#### Food Nutrition Microservice
The team project, "Nutrition Guide," is a dynamic, auto-scaling web application microservice that interfaces with the Databricks data pipeline, utilizing Docker for effective containerization and Azure App Services and Flask for deployment and management.

Explore the user journey within the Nutrition Guide application:

1. Nutrient Selection: Users select their desired nutrient from a drop-down menu within the web application.
2. Nutrition Query: Upon selection, the application processes this input and searches its database to identify the top 10 foods that are highest in the chosen nutrient.
3. Results Display: The web application then displays a detailed list of these top 10 foods, providing users with valuable information about their nutrient content.


[Project Link](https://github.com/nogibjj/DE_Final_Project_Nutrition_Guide_Food)



#### ETL Pipeline with Query Tool
The goal of this project is to create ETL using Rust and query the SQLite database. Copilot is used to produce the Rust code.

**Functionality**
1. Extract a dataset from a URL with CSV format.
2. Transform the data by cleaning, filtering, enriching, etc to get it ready for analysis.
3. Load the transformed data into a SQLite database table using Python's sqlite3 module with create and update operations.
4. Accept and execute general SQL queries including in CRUD (Create, Read, Update, Delete) operations on the SQLite database to analyze and retrieve insights from the data.

[Project Link](https://github.com/nogibjj/xueqing_wu_individual2)